import Dependencies._

lazy val scala_interview = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "parallelai",
      scalaVersion := "2.12.3",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "scala_interview",
    libraryDependencies += scalaTest % Test
  )
