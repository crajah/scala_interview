package parallelai.interview

import scala.io.Source

/**
  * Scala Test Level 3 (Advanced)
  *
  * Using a Scala code generation framework (Hint: Scala Macros)
  *
  * Generate the code you wrote in Scala Test level 2 from the JSON
  * from the file resources/codegen.json
  */

trait ScalaInterviewLvl3 {
  val json_str = Source.fromResource("codegen.json").getLines.mkString

}
